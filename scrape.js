var url = process.argv[2];
scrape(url, function(err, article, meta) {
  // Main Article
  // console.log(article.content.text());

  // // Title
  console.log(article.title);

  // Article HTML Source Code
  fs.writeFile(article.title.replace(/ /g,'_').replace(/$/,'.html'), article.content.html(), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
});
});
