/*\
title: $:/core/modules/widgets/action-setfield.js
type: application/javascript
module-type: widget

Action widget to set a single field or index on a tiddler.

\*/
(function() {

    /*jslint node: true, browser: true */
    /*global $tw: false */
    "use strict";

    var Widget = require("$:/core/modules/widgets/widget.js").widget;

    var SetFieldWidget = function(parseTreeNode, options) {
        this.initialise(parseTreeNode, options);
    };

    /*
    Inherit from the base widget class
    */
    SetFieldWidget.prototype = new Widget();

    /*
    Render this widget into the DOM
    */
    SetFieldWidget.prototype.render = function(parent, nextSibling) {
        this.computeAttributes();
        this.execute();
    };

    /*
    Compute the internal state of the widget
    */
    SetFieldWidget.prototype.execute = function() {
        this.element = this.getAttribute("$element");
    };

    /*
    Refresh the widget by ensuring our attributes are up to date
    */
    SetFieldWidget.prototype.refresh = function(changedTiddlers) {
        var changedAttributes = this.computeAttributes();
        if (changedAttributes["$tiddler"] || changedAttributes["$field"] || changedAttributes["$index"] || changedAttributes["$value"]) {
            this.refreshSelf();
            return true;
        }
        return this.refreshChildren(changedTiddlers);
    };

    /*
    Invoke the action associated with this widget
    */
    SetFieldWidget.prototype.invokeAction = function(triggeringWidget, event) {
        var tiddlertitle = this.getVariable("currentTiddler");
        var range = window.getSelection().getRangeAt(0);
        var selectionContents = range.extractContents();
        var element = document.createElement(this.element);
        $tw.utils.each(this.attributes, function(attribute, name) {
            if (name.charAt(0) !== "$") {
                element.setAttribute(name, attribute)
            }
        });
        element.appendChild(selectionContents);
        range.insertNode(element);
        var tiddlertext = document.querySelector("[data-tiddler-title=" + CSS.escape(tiddlertitle) + "]").getElementsByClassName("tc-tiddler-body")[0].innerHTML


        var self = this,
            options = {};
        this.wiki.setText(tiddlertitle, "text",null, tiddlertext, options);
        return true; // Action was invoked
    };

    exports["action-mark"] = SetFieldWidget;

})();