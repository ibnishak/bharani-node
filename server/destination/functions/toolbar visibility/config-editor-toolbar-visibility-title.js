/*\
title: $:/core/modules/macros/config-editor-toolbar-visibility-title.js
type: application/javascript
module-type: macro

Macro to return a formatted version of the current time

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

/*
Information about this macro
*/

exports.name = "config-editor-toolbar-visibility-title";

exports.params = [];

/*
Run the macro
*/
exports.run = function() {
       var breakpoint = parseInt(this.wiki.getTiddlerText("$:/themes/tiddlywiki/vanilla/metrics/sidebarbreakpoint"), 10);
var title;
      if (window.screen.width >= breakpoint) {
  // resolution is  above breakpoint
       title = "$:/config/EditorToolbarButtons/Visibility/AboveBreakpoint/" + this.getVariable("currentTiddler")
} else {
title = "$:/config/EditorToolbarButtons/Visibility/BelowBreakpoint/" + this.getVariable("currentTiddler")
}
	return title;
};

})();
