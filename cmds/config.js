const config = require('../config.json')
const configFile = 'config.json';
const init = require('../server/destination/init.json')
const initFile = 'server/destination/init.json'
const fs =  require('fs')

module.exports = (args) => {
    if (args.d || args.directory) {
    	console.log(config)
        config.path = args.d
        init.wikiTiddlersSubDir = args.d
        fs.writeFile(configFile, JSON.stringify(config, null, 2), function(err) {
            if (err) return console.log(err);
            console.log('writing to ' + configFile);
        });
        fs.writeFile(initFile, JSON.stringify(init, null, 2), function(err) {
            if (err) return console.log(err);
            console.log('writing to ' + initFile);
        });
    }
}