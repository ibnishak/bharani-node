const path = require('path')
const fs = require('fs')
const options = {
    slient: false,
    detached: false
};

module.exports = (args) => {

    var childProcess = require('child_process');

    function runScript(scriptPath, callback) {
        if (args.detached) {
            options.detached = true
        }
        // keep track of whether callback has been invoked to prevent multiple invocations
        var invoked = false;

        var process = childProcess.fork(scriptPath, [path.resolve(__dirname,"../server/destination"), "--server"], options);

        // listen for errors as they may prevent the exit event from firing
        process.on('error', function(err) {
            if (invoked) return;
            invoked = true;
            callback(err);
        });

        // execute the callback once the process has finished running
        process.on('exit', function(code) {
            if (invoked) return;
            invoked = true;
            var err = code === 0 ? null : new Error('exit code ' + code);
            callback(err);
        });

    }

    // Now we can run a script and invoke a callback when complete, e.g.
    runScript(path.resolve(__dirname, '../server/tiddlywiki.js'), function(err) {
        console.log('\n\n\x1b[36m%s\x1b[0m\n%s', 'Port 8080 occupied. Are you sure server is not already running?', 'Open http://localhost:8080');
    });
}