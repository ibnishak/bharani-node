const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const r = require('readability-node');
const fs = require('fs')
const path = require('path');
const config = require('../config.json')
const templates = require('../utils/templates.js')
const pretty = require('pretty')

var TemplateEngine = function(tpl, data) {
    var re = /<%([^%>]+)?%>/g,
        match;
    while (match = re.exec(tpl)) {
        tpl = tpl.replace(match[0], data[match[1]])
    }
    return tpl;
}



module.exports = (args) => {
    try {
        const url = args._[1].toString();
        JSDOM.fromURL(url).then(dom => {
            var article = new r.Readability(url, dom.window.document).parse();

            var content = TemplateEngine(templates.tw, {
                title: article.title,
                text: pretty(article.content.toString()),
                source: url,
                type: "text/vnd.tiddlywiki"
            });
            var filepath = config.path.toString();
            if (args.d || args.directory) {
                filepath = args.d.toString();
            }
            fs.writeFile(path.join(filepath, article.title.replace(/ /g, '_').replace(/$/, '.tid')), content, { flag: "wx" }, function(err) {
                if (err) {
                    if (err.code == "EEXIST") {
                        var err = "File with that title already exists. Please rename the file";
                    }
                    if (err.code == "ENOENT") {
                        var err = "Your destination directory cannot be found. Please create your destination directory";
                    }
                    return console.log(err);
                }

                console.log("The file was saved!");
            });
        });

        console.log(`Waiting to be written`)
    } catch (err) {
        console.error(err)
    }
}