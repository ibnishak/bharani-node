const menus = {
  main: `
    bhar [command] <options>

    add................. add url to the collection
    config.............. change the configurations, like article folder, export folder etc
    version ............ show package version
    help ............... show help menu for a command`,

  add: `
    bhar add <url>

    eg: bhar add https://en.wikipedia.org/wiki/Wikipedia`,

  config: `
    outside forecast <options>

    --location, -l ..... the location to use`
}

module.exports = (args) => {
  const subCmd = args._[0] === 'help'
    ? args._[1]
    : args._[0]

  console.log(menus[subCmd] || menus.main)
}
